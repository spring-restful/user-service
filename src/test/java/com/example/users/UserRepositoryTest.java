package com.example.users;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.example.users.repogitory.UserEntity;
import com.example.users.repogitory.UserRepository;

import lombok.extern.slf4j.Slf4j;

@DataJpaTest
//@Slf4j
public class UserRepositoryTest {

	@Autowired
	UserRepository userRepository;
	
	@Test
	void regist() {
		//before
		UserEntity user = UserEntity.builder()
				.email("spring@example.com")
				.userId("test1234")
				.encryptedPassword("test1234")
				.build();
//		log.debug(user.toString());
		//when
		UserEntity result = userRepository.save(user);
		Assertions.assertNotNull(result);
	}
}
