package com.example.users.util;

import com.example.users.dto.UserDto;
import com.example.users.repogitory.UserEntity;
import com.example.users.vo.RequestUser;
import com.example.users.vo.ResponseUser;

public class ObjectMapperUtil {
	
	//RequestUser -> UserDto
	public static UserDto requestUserToUserDto(RequestUser requestUser) {
		return UserDto.builder()
				.name(requestUser.getName())
				.email(requestUser.getEmail())
				.password(requestUser.getPassword())
				.build();
	}
	
	//UserDto -> ResponseUser
	public static ResponseUser userDtoToResponseUser(UserDto userDto) {
		return ResponseUser.builder()
				.userId(userDto.getUserId())
				.email(userDto.getEmail())
				.name(userDto.getName())
				.build();
	}
	
	//UserDto -> UserEntity
	public static UserEntity userDtoToUserEntity(UserDto userDto) {
		return UserEntity.builder()
		.email(userDto.getEmail())
		.name(userDto.getName())
		.userId(userDto.getUserId())
		.encryptedPassword(userDto.getEncryptedPassword())
		.build();
	}
	
	
	//UserEntity -> UserDto
	public static UserDto userEntityToUserDto(UserEntity userEntity) {
		return UserDto.builder()
				.email(userEntity.getEmail())
				.name(userEntity.getName())
				.userId(userEntity.getUserId())
				.encryptedPassword(userEntity.getEncryptedPassword())
				.build();
	}

}
