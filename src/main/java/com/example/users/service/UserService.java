package com.example.users.service;

import java.util.List;

import com.example.users.dto.UserDto;
import com.example.users.exception.UnAuthenticationException;

public interface UserService {
	/**
	 * 사용자 등록 
	 * @param userDto
	 * @return userDto : UserDto
	 */
	public UserDto regist(UserDto userDto);
	
	/**
	 * 사용자 목록
	 * @return users : List<UserDto> user 목록
	 */
	public List<UserDto> getUsers();
	/**
	 * 사용자 상세 정보
	 * @param userId
	 * @return userDto : UserDto
	 */
	public UserDto getUser(String userId);
	/**
	 * 사용자 정보 삭제
	 * @param userId
	 * @return userDto : UserDto
	 */
	public void remove(String userId);
	/**
	 * 사용자 정보 수정
	 * @param userDto
	 * @return userDto : UserDto
	 */
	public UserDto modify(UserDto userDto);
	
	/**
	 * 로그인
	 * @param user
	 * @return userDto : UserDto
	 * @throws UnAuthenticationException 
	 */
	public UserDto loginCheck(UserDto user) throws UnAuthenticationException ;
	
}
