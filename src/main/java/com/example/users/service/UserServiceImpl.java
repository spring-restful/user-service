package com.example.users.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.example.users.dto.UserDto;
import com.example.users.exception.UnAuthenticationException;
import com.example.users.repogitory.UserEntity;
import com.example.users.repogitory.UserRepository;
import com.example.users.util.ObjectMapperUtil;

@Service
public class UserServiceImpl implements UserService {
	UserRepository userRepository;
	
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * 사용자 등록 
	 * UUID.randomUUID로 userId생성
	 * password 암호화해서 encryptedPassword 생성
	 * userDto ->userEntity로 변환해서 reposiotry 호출
	 * @param userDto
	 * @return 
	 */
	@Override
	public UserDto regist(UserDto userDto) {
		//controller userDto(email, name, password)
		// userid, encryptedPassword 
		userDto.setUserId(UUID.randomUUID().toString());
		//bcrypt로  암호화해서
		userDto.setEncryptedPassword( BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt()));  
		//user dto->userEntity변환
		userRepository.save(ObjectMapperUtil.userDtoToUserEntity(userDto));
		return userDto;
	}

	@Override
	public List<UserDto> getUsers() {
		List<UserDto> users = new ArrayList<>();
		userRepository.findAll().forEach(
				entity -> users.add(ObjectMapperUtil.userEntityToUserDto(entity))
		);
		return users;
	}

	@Override
	public UserDto getUser(String userId) {
		return ObjectMapperUtil.userEntityToUserDto(
				userRepository.findByUserId(userId)
		);
	}

	@Override
	public void remove(String userId) {
		userRepository.delete( userRepository.findByUserId(userId));
	}

	@Override
	public UserDto modify(UserDto userDto) {
		UserEntity userEntity = userRepository.findByUserId(userDto.getUserId());
		userEntity.setEmail(userDto.getEmail());
		userEntity.setName(userDto.getName());
		userEntity.setEncryptedPassword(BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt()));
		
		userRepository.save(userEntity);
		
		return ObjectMapperUtil.userEntityToUserDto(userEntity);
	}

	@Override
	public UserDto loginCheck(UserDto user) 
			throws UnAuthenticationException {
		// user email로 검색 repository-> encryptedPassword 체크
		UserEntity userEntity= 
				userRepository.findByEmail(user.getEmail());
		
		if(userEntity !=null 
				&& BCrypt.checkpw(user.getPassword(), userEntity.getEncryptedPassword())) {
			return ObjectMapperUtil.userEntityToUserDto(userEntity);
		}
		throw new UnAuthenticationException();
	}

}
