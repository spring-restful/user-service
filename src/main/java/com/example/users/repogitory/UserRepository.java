package com.example.users.repogitory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
	UserEntity findByUserId(String userId);  //select * from users where user_id=?
//	List<UserEntity> findByName(String name); //select * from users where name =?
	UserEntity findByEmail(String email); //select * from users where email=? 
}
