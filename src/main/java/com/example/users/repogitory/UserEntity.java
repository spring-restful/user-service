package com.example.users.repogitory;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="users")
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false, length = 50, unique = true)
	private String email;
	@Column(length = 50)
	private String name;
	@Column(name="user_id", nullable = false, unique = true)
	private String userId;
//	@Column(name="create_at")
//	private LocalDate createAt;
	@Column(name = "encrypted_password", unique = false, nullable = false)
	private String encryptedPassword;
	
	@Builder
	public UserEntity(String email, String name, String userId,  String encryptedPassword) {
		this.email = email;
		this.name = name;
		this.userId = userId;
		this.encryptedPassword = encryptedPassword;
	}
	
	
}
