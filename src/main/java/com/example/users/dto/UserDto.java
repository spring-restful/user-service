package com.example.users.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
	private String email;
	private String name;
	private String password;
	private String userId;
	private String encryptedPassword;
	
	
//	@Builder
//	public UserDto(String email, String name, String password) {
//		super();
//		this.email = email;
//		this.name = name;
//		this.password = password;
//	}

	@Builder
	public UserDto(String email, String name, String password, String userId, String encryptedPassword) {
		this.email = email;
		this.name = name;
		this.password = password;
		this.userId = userId;
		this.encryptedPassword = encryptedPassword;
	}

	
}
